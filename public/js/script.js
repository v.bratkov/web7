$('.myClass').slick({
    dots: true,
    infinite: false,
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [
        {
            breakpoint: 568,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
            }
        }]
});